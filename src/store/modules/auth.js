const state = {
    isAuth: JSON.parse(localStorage.getItem('AUTH')),
};

const getters = {
    isAuth: state => state.isAuth,
};

const mutations = {
    setAuth(state, data) {
        localStorage.setItem('AUTH', JSON.stringify(data));
        state.isAuth = data;
    },
};

const actions = {
    LOGIN({commit}) {
        commit('setAuth', true);
    },
    LOGOUT({commit}) {
        commit('setAuth', false);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};