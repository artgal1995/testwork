const state = {
    modalCurrent: '',
    modalPayload: '',
};

const getters = {
    isShowModal(state) {
        return !!state.modalCurrent;
    },
    getModalPayload(state) {
        return state.modalPayload;
    }
};

const mutations = {
    setModalCurrent(state, modalName) {
        state.modalCurrent = modalName;
    },
    setModalPayload(state, payload) {
        state.modalPayload = payload;
    },
};

const actions = {
    SHOW_MODAL({commit}, {name, payload = {}}) {
        commit('setModalCurrent', name);
        commit('setModalPayload', payload);
    },
    CLOSE_MODAL({commit}) {
        commit('setModalCurrent', null);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
