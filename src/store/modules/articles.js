const state = {
    articles: JSON.parse(localStorage.getItem('articles')),
};

const getters = {
    getArticles: state => {
        return state.articles.sort((a, b) => new Date(b.dateCreated) - new Date(a.dateCreated));
    },
};

const mutations = {
    editArticles(state, data) {
        localStorage.setItem('articles', JSON.stringify(data));
        state.articles = data;
    },
};

const actions = {
    CREATE_ARTICLE({commit}, data) {
        const articles = state.articles && state.articles.length > 0 ? [...state.articles, data] : [data];
        commit('editArticles', articles);
    },
    EDIT_ARTICLE({commit}, data) {
        const articles = state.articles.map((article) => {
            let value = article;

            if (article.id === data.id) {
                value = data
            }

            return value;
        });
        commit('editArticles', articles);
    },
    DELETE_ARTICLE({commit}, id) {
        const articles = state.articles.filter((article) => article.id !== id);
        commit('editArticles', articles);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};