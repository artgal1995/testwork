import Vue from 'vue';
import Router from 'vue-router';
import store from '@/store/';

Vue.use(Router);

const requireNoAuthenticated = (to, from, next) => {
    store.getters['auth/isAuth'] ? next() : next('/login');
};

const requireAuthenticated = (to, from, next) => {
    store.getters['auth/isAuth'] ? next('/edit') : next();
};

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Main',
            meta: {
                title: 'Главная',
            },
            component: () => import('../pages/Main'),
        },
        {
            path: '/login',
            name: 'Login',
            meta: {
                title: 'Вход',
            },
            component: () => import('../pages/Login'),
            beforeEnter: requireAuthenticated,
        },
        {
            path: '/edit',
            name: 'Edit',
            meta: {
                title: 'Редактирование статей',
            },
            component: () => import('../pages/Edit'),
            beforeEnter: requireNoAuthenticated,
        },
    ]
});
